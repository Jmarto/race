<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230117224353 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE race (id UUID NOT NULL, title VARCHAR(255) NOT NULL, date DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN race.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE result (id UUID NOT NULL, race_id UUID NOT NULL, full_name VARCHAR(255) NOT NULL, distance VARCHAR(255) NOT NULL, time INT NOT NULL, age_category VARCHAR(255) NOT NULL, overall_placement INT DEFAULT NULL, age_category_placement INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_136AC1136E59D40D ON result (race_id)');
        $this->addSql('COMMENT ON COLUMN result.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN result.race_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE result ADD CONSTRAINT FK_136AC1136E59D40D FOREIGN KEY (race_id) REFERENCES race (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE result DROP CONSTRAINT FK_136AC1136E59D40D');
        $this->addSql('DROP TABLE race');
        $this->addSql('DROP TABLE result');
    }
}
