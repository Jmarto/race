<?php

namespace App\Controller;

use App\Model\InputRaceDto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

final class PostFileRaceAction extends AbstractController
{
    public function __invoke(Request $request): InputRaceDto
    {
        $uploadedFile = $request->files->get('csvFile');
        $inputRace = new InputRaceDto();
        $inputRace->csvFile = $uploadedFile;
        $inputRace->title = $request->get('title');
        $inputRace->date = \DateTime::createFromFormat('Y-m-d', $request->get('date'));

        return $inputRace;
    }
}