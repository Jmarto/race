<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use App\Controller\PostFileRaceAction;
use App\Model\InputRaceDto;
use App\Model\OutputRaceDto;
use App\Repository\RaceRepository;
use App\State\RaceProvider;
use App\State\RaceProcessor;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity(repositoryClass: RaceRepository::class)]
#[ApiResource]
#[
    Post(
        inputFormats: ['multipart' => ['multipart/form-data']],
        controller: PostFileRaceAction::class,
        input: InputRaceDto::class,
        deserialize: false,
        processor: RaceProcessor::class,
    ),
    GetCollection(
        output: OutputRaceDto::class,
        provider: RaceProvider::class
    ),
    ApiFilter(
        SearchFilter::class, properties: ['title' => 'partial'],
    ),
    ApiFilter(
        OrderFilter::class
    )
]
class Race
{
    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    private ?Uuid $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    private ?\DateTimeInterface $date = null;

    #[ORM\OneToMany(mappedBy: 'race', targetEntity: Result::class, cascade: ['all'], orphanRemoval: true)]
    private Collection $results;

    public function __construct()
    {
        $this->results = new ArrayCollection();
    }

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection<int, Result>
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    public function addResult(Result $result): self
    {
        if (!$this->results->contains($result)) {
            $this->results->add($result);
            $result->setRace($this);
        }

        return $this;
    }

    public function removeResult(Result $result): self
    {
        if ($this->results->removeElement($result)) {
            // set the owning side to null (unless already changed)
            if ($result->getRace() === $this) {
                $result->setRace(null);
            }
        }

        return $this;
    }

    public function calculateOverallPlacements(DistanceType $distanceType = DistanceType::LONG): static
    {
        $longDistanceResults = $this->results->filter(fn($result) => $result->getDistance() === $distanceType);
        $longDistanceResults = $longDistanceResults->toArray();

        usort($longDistanceResults, fn($a, $b) => $a->getTime() <=> $b->getTime());

        array_walk(
            $longDistanceResults,
            fn($result, $i) => $result->setOverallPlacement($i + 1)
        );
        return $this;
    }

    public function calculateAgeCategoryPlacements(DistanceType $distanceType = DistanceType::LONG): static
    {
        $longDistanceResults = $this->results->filter(fn($result) => $result->getDistance() === $distanceType);
        $longDistanceResults = $longDistanceResults->toArray();

        $ageCategories = array_reduce($longDistanceResults, function ($carry, $result) {
            if (!isset($carry[$result->getAgeCategory()])) {
                $carry[$result->getAgeCategory()] = [];
            }
            $carry[$result->getAgeCategory()][] = $result;
            return $carry;
        }, []);

        foreach ($ageCategories as $k => $category) {
            usort($category, fn($a, $b) => $a->getTime() <=> $b->getTime());
            array_walk(
                $category,
                fn($currentResult, $i) => $currentResult->setAgeCategoryPlacement($i + 1)
            );
        }

        return $this;
    }

}
