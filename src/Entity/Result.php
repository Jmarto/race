<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Link;
use ApiPlatform\Metadata\Put;
use App\Model\OutputResultDto;
use App\Repository\ResultRepository;
use App\State\ResultProvider;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

enum DistanceType: string
{
    case MEDIUM = 'medium';
    case LONG = 'long';
}

#[ORM\Entity(repositoryClass: ResultRepository::class)]
#[ApiResource(
    uriTemplate: '/races/{id}/results',
    operations: [
        new GetCollection(
            input: OutputResultDto::class,
            provider: ResultProvider::class,
        ),
    ],
    uriVariables: [
        'id' => new Link(toProperty: 'race', fromClass: Race::class),
    ])]
#[
    Put(
        normalizationContext: ['groups' => ['put']],
        denormalizationContext: ['groups' => ['put']],
    ),
    ApiFilter(
        OrderFilter::class
    ),
    ApiFilter(
        SearchFilter::class, properties: ['fullName' => 'partial'],
    ),
]
class Result
{
    #[ORM\Id]
    #[ORM\Column(type: UuidType::NAME, unique: true)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: 'doctrine.uuid_generator')]
    #[Groups(['put'])]
    private ?Uuid $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['put'])]
    private ?string $fullName = null;

    #[ORM\Column(type: 'string', enumType: DistanceType::class)]
    #[Groups(['put'])]

    /**
     * @extends DistanceType
     */
    private ?DistanceType $distance = null;

    #[ORM\Column(type: 'integer')]
    #[Groups(['put'])]
    private ?int $time = null;

    #[ORM\Column(length: 255)]
    #[Groups(['put'])]
    private ?string $ageCategory = null;

    #[ORM\ManyToOne(inversedBy: 'results')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['put'])]
    private ?Race $race = null;

    #[ORM\Column(nullable: true)]
    private ?int $overallPlacement = null;

    #[ORM\Column(nullable: true)]
    private ?int $ageCategoryPlacement = null;

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getDistance(): ?DistanceType
    {
        return $this->distance;
    }

    public function setDistance(DistanceType $distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getTime(): ?int
    {
        return $this->time;
    }

    /**
     * @param int|null $time
     * @return Result
     */
    public function setTime(?int $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getAgeCategory(): ?string
    {
        return $this->ageCategory;
    }

    public function setAgeCategory(string $ageCategory): self
    {
        $this->ageCategory = $ageCategory;

        return $this;
    }

    public function getRace(): ?Race
    {
        return $this->race;
    }

    public function setRace(?Race $race): self
    {
        $this->race = $race;

        return $this;
    }

    public function getOverallPlacement(): ?int
    {
        return $this->overallPlacement;
    }

    public function setOverallPlacement(?int $overallPlacement): self
    {
        $this->overallPlacement = $overallPlacement;

        return $this;
    }

    public function getAgeCategoryPlacement(): ?int
    {
        return $this->ageCategoryPlacement;
    }

    public function setAgeCategoryPlacement(?int $ageCategoryPlacement): self
    {
        $this->ageCategoryPlacement = $ageCategoryPlacement;

        return $this;
    }
}
