<?php

namespace App\Model;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
final class InputRaceDto
{
    #[Assert\NotBlank]
    public ?string $title;


    #[Assert\Type(\DateTimeInterface::class)]
    public ?\DateTimeInterface $date;


    #[Assert\Type(UploadedFile::class)]
    public ?File $csvFile;
}