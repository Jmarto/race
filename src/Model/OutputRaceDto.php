<?php

namespace App\Model;

final class OutputRaceDto
{
    public ?string $title;

    public ?string $date;
    public ?string $avgMediumFinishTime;

    public ?string $avgLongFinishTime;
}