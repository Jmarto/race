<?php

namespace App\Model;

class OutputResultDto
{
    public ?string $fullName;
    public ?string $finishTime;

    public ?string $distance;
    public ?string $ageCategory;
    public ?string $overallPlace;
    public ?string $ageCategoryPlace;
}