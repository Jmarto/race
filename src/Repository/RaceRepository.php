<?php

namespace App\Repository;

use App\Entity\DistanceType;
use App\Entity\Race;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Race>
 *
 * @method Race|null find($id, $lockMode = null, $lockVersion = null)
 * @method Race|null findOneBy(array $criteria, array $orderBy = null)
 * @method Race[]    findAll()
 * @method Race[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Race::class);
    }

    public function save(Race $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Race $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getAvgFinishTimeByDistance(): ?iterable
    {
        $qb = $this->createQueryBuilder('r');

        $qb->select('r.id, AVG(CASE WHEN result.distance = :medium THEN result.time ELSE 0 END) as avg_time_medium,
        AVG(CASE WHEN result.distance = :long THEN result.time ELSE 0 END) as avg_time_long')
            ->join('r.results', 'result')
            ->groupBy('r.id')
            ->setParameter('medium', DistanceType::MEDIUM->value)
            ->setParameter('long', DistanceType::LONG->value);

        return $qb->getQuery()->getArrayResult();
    }
}
