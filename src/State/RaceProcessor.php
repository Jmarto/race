<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\DistanceType;
use App\Entity\Race;
use App\Entity\Result;
use Symfony\Component\Serializer\SerializerInterface;

final class RaceProcessor implements ProcessorInterface
{

    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly ProcessorInterface $persistProcessor,
    ) {
    }

    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        /**
         * Transform DTO InputRaceDto into Race Entity
         */
        $race = new Race();
        $race->setTitle($data->title);
        $race->setDate($data->date);

        $exportData = $this->serializer->decode($data->csvFile->getContent(), 'csv');

        /**
         * Process Race Results data on Result entity en set the actual Race
         */
        $this->processResultsDataForRace($exportData, $race);
        $race
            ->calculateOverallPlacements()
            ->calculateAgeCategoryPlacements();

        return $this->persistProcessor->process($race, $operation, $uriVariables, $context);
    }

    /**
     * @param $exportData
     * @param Race $race
     * @return void
     */
    private function processResultsDataForRace($exportData, Race $race): void
    {
        foreach ($exportData as $resultData) {
            $result = new Result();
            $time = strtotime($resultData['time']) - strtotime('TODAY');

            $result
                ->setDistance(DistanceType::from($resultData['distance']))
                ->setTime($time)
                ->setAgeCategory($resultData['ageCategory'])
                ->setFullName($resultData['fullName']);
            $race->addResult($result);
        }
    }
}