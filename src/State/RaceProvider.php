<?php

namespace App\State;

use ApiPlatform\Doctrine\Orm\Paginator;
use ApiPlatform\Doctrine\Orm\State\CollectionProvider;
use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Race;
use App\Model\OutputRaceDto;
use Doctrine\ORM\EntityManagerInterface;

final class RaceProvider implements ProviderInterface
{
    private CollectionProvider $collectionProvider;
    private EntityManagerInterface $manager;

    public function __construct(CollectionProvider $collectionProvider, EntityManagerInterface $manager)
    {
        $this->collectionProvider = $collectionProvider;
        $this->manager = $manager;
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        if ($operation instanceof CollectionOperationInterface) {
            /** @var Paginator $collection */
            $collection = $this->collectionProvider->provide($operation, $uriVariables, $context);
            $avgFinishTimeByDistance = $this->manager->getRepository(Race::class)->getAvgFinishTimeByDistance();
            $outputRaces = [];

            $avgFinishTimeByDistance = array_reduce($avgFinishTimeByDistance, function($carry, $item) {
                $carry[$item['id']->toRfc4122()] = $item;
                return $carry;
            }, []);

            /** @var Race $race */
            foreach ($collection as $race) {
                $outputRace = new OutputRaceDto();
                $outputRace->title = $race->getTitle();
                $outputRace->date = $race->getDate()->format('Y-m-d');

                $raceId = $race->getId()->toRfc4122();

                if (isset($avgFinishTimeByDistance[$raceId])) {
                    $outputRace->avgMediumFinishTime = date('H:i:s', $avgFinishTimeByDistance[$raceId]['avg_time_medium']);
                    $outputRace->avgLongFinishTime = date('H:i:s', $avgFinishTimeByDistance[$raceId]['avg_time_long']);
                }
                $outputRaces[$raceId] = $outputRace;
            }
            return $outputRaces;
        }
        return null;
    }
}
