<?php

namespace App\State;

use ApiPlatform\Doctrine\Orm\Paginator;
use ApiPlatform\Doctrine\Orm\State\CollectionProvider;
use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Race;
use App\Entity\Result;
use App\Model\OutputRaceDto;
use App\Model\OutputResultDto;
use Doctrine\ORM\EntityManagerInterface;

final class ResultProvider implements ProviderInterface
{
    private CollectionProvider $collectionProvider;


    public function __construct(CollectionProvider $collectionProvider)
    {
        $this->collectionProvider = $collectionProvider;

    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        if ($operation instanceof CollectionOperationInterface) {
            /** @var Paginator $collection */
            $collection = $this->collectionProvider->provide($operation, $uriVariables, $context);
            $outputResults = [];

            /** @var Result $result */
            foreach ($collection as $result) {
                $outputResult = new OutputResultDto();
                $outputResult->distance = $result->getDistance()->value;
                $outputResult->fullName = $result->getFullName();
                $outputResult->finishTime = date('H:i:s',$result->getTime());
                $outputResult->ageCategory = $result->getAgeCategory();
                $outputResult->overallPlace = $result->getOverallPlacement();
                $outputResult->ageCategoryPlace = $result->getAgeCategoryPlacement();

                $outputResults[] = $outputResult;
            }
            return $outputResults;
        }
        return null;
    }
}
